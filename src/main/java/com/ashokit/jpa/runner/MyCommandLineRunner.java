package com.ashokit.jpa.runner;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.ashokit.jpa.entities.ProductEntity;
import com.ashokit.jpa.entities.VendorEntity;
import com.ashokit.jpa.repository.VendorEntityRepository;

@Component
public class MyCommandLineRunner implements CommandLineRunner {
	@Autowired
	VendorEntityRepository  vendorRepo;
    
	@Transactional
	@Override
	public void run(String... args) throws Exception {
		
		/*
		VendorEntity vEntity = new VendorEntity();
		vEntity.setVendorId(102);  vEntity.setVendorName("Oracle");
		
		ProductEntity pEntity1 = new ProductEntity();
		pEntity1.setProductId(20001); pEntity1.setProductName("Weblogic");
		ProductEntity pEntity2 = new ProductEntity();
		pEntity2.setProductId(20002); pEntity2.setProductName("JDeveloper");
		
		List<ProductEntity>  productsList = Arrays.asList(pEntity1, pEntity2);
		vEntity.setProductsList(productsList);
		
		vendorRepo.save(vEntity);
		*/
		
		/*
		Optional<VendorEntity> opt = vendorRepo.findById(101);
		if(opt.isPresent()) {
			VendorEntity vE = opt.get();
			System.out.println(vE.getVendorId()+" : "+ vE.getVendorName());
			List<ProductEntity> lstProducts = vE.getProductsList();
			lstProducts.forEach(System.out::println);
		}
		*/
		
		
		Optional<VendorEntity> opt = vendorRepo.findById(101);
		if(opt.isPresent()) {
			VendorEntity vE = opt.get();
			List<ProductEntity> lstProducts = vE.getProductsList();
			Iterator<ProductEntity> i = lstProducts.iterator();
			while(i.hasNext()) {
				ProductEntity pe = i.next();
				if(pe.getProductId()== 10003) {
					i.remove();
				}
			}
		}
		

	}

}

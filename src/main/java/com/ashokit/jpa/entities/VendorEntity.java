package com.ashokit.jpa.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table( name = "VENDOR" )
@Data
public class VendorEntity {
	@Id
	@Column( name = "VEN_ID")
	private Integer vendorId;
	
	@Column( name = "VEN_NAME" , length = 20 )
	private String vendorName;
	
	@OneToMany( cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
	@JoinColumn ( name = "VEN_ID_FK" )
	private List<ProductEntity> productsList;
	

}

package com.ashokit.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ashokit.jpa.entities.VendorEntity;

public interface VendorEntityRepository extends JpaRepository<VendorEntity, Integer> {

}
